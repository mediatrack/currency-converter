<?php
namespace Mediatrack\CurrencyConverter\Model;

/**
 * Currency Converter Rate Model
 *
 * @package  Mediatrack\CurrencyConverter
 * @author   Jakub Bronarski <jakub.bronarski@mediatrack.pl>
 */
class Rate
{
    /**
     * @var string
     */
    const CURRENCY_CONVERTER_URL = 'https://free.currencyconverterapi.com/api/v6/convert?q=RUB_PLN&compact=y';

    /**
     * @var \Magento\Framework\HTTP\ZendClientFactory
     */
    protected $httpClientFactory;

    /**
     * Constructor
     *
     * @param \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory
     */
    public function __construct(
        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory
    ) {
        $this->httpClientFactory = $httpClientFactory;
    }

    /**
     * Get rate
     *
     * @return string
     * @throws \Exception
     */
    public function getRate()
    {
        /** @var array */
        $result = $this->fetchFromApi();
        if (isset($result['RUB_PLN']['val'])) {
            return $result['RUB_PLN']['val'];
        } else {
            throw new \Exception(__('Invalid rate'));
        }
    }

    /**
     * Fetch rate from API
     *
     * @return array
     * @throws Zend_Http_Client_Exception
     */
    protected function fetchFromApi()
    {
        /** @var \Magento\Framework\HTTP\ZendClient $httpClient */
        $httpClient = $this->httpClientFactory->create();

        /** @var string */
        $jsonResponse = $httpClient->setUri(
            self::CURRENCY_CONVERTER_URL
        )->request(
            'GET'
        )->getBody();

        return json_decode($jsonResponse, true);
    }
}
