define([
    'ko',
    'jquery',
    'uiComponent',
    'Mediatrack_CurrencyConverter/js/action/convert'
], function (ko, $, Component, convertAction) {
    'use strict';

    var result = ko.observable('-');

    return Component.extend({
        convert: function () {
            this.onChange();
            var input = $('#currency-converter-input'),
                button = $('#currency-converter-button');

            input.prop('disabled', true);
            button.prop('disabled', true);

            convertAction(input.val(), result).then(function () {
                input.prop('disabled', false);
                button.prop('disabled', false);
            });
        },
        onChange: function () {
            if (result() != '-') {
                result('-');
            }
        },
        getResult: function () {
            return result();
        }
    });
});
