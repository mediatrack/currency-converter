# Currency Converter

Magento module to convert currency from RUB to PLN

## Installation

```bash
composer config repositories.currency-converter-mediatrack git git@bitbucket.org:mediatrack/currency-converter.git
composer require mediatrack/currency-converter
```

## Usage

Open STORE_URL/currencyconverter
