/*global define,alert*/
define(
    [
        'ko',
        'jquery',
        'mage/storage',
        'mage/translate',
    ],
    function (
        ko,
        $,
        storage,
        $t
    ) {
        'use strict';
        return function (value, result) {
            return storage.post(
                'currencyconverter/index/convert',
                {from: value},
                true,
                'application/x-www-form-urlencoded'
            ).done(
                function (response) {
                    if (response.result) {
                        result(response.result);
                    } else if (response.error) {
                        alert(response.error);
                    } else {
                        alert($t('Error occured. Try again later.'));
                    }
                }
            ).fail(
                function (response) {
                    alert($t('Error occured. Try again later.'));
                }
            );
        };
    }
);