<?php
namespace Mediatrack\CurrencyConverter\Controller\Index;

/**
 * Currency Converter Convert Controller
 *
 * @package  Mediatrack\CurrencyConverter
 * @author   Jakub Bronarski <jakub.bronarski@mediatrack.pl>
 */
class Convert extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Mediatrack\CurrencyConverter\Model\Rate
     */
    protected $_rateService;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $_resultJsonFactory;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Mediatrack\CurrencyConverter\Model\Rate $rateService
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Mediatrack\CurrencyConverter\Model\Rate $rateService,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
    )
    {
        $this->_rateService = $rateService;
        $this->_resultJsonFactory = $resultJsonFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $from = $this->getRequest()->getPostValue('from');

        if (!is_numeric($from)) {
            return $this->_resultJsonFactory->create()->setData(['error' => __('Invalid value')]);
        }

        try {
            /** @var float */
            $result = number_format($from * $this->_rateService->getRate(), 2, ',', '');

            return  $this->_resultJsonFactory->create()->setData(['result' => $result]);
        } catch (\Exception $e) {
            return  $this->_resultJsonFactory->create()->setData(['error' => $e->getMessage()]);
        }
    }
}